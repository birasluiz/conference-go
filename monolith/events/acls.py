from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, headers=headers, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return content["photos"][0]["url"]
    except (KeyError, IndexError):
        return {"photo_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    geocode_response = requests.get(geocode_url)
    print(geocode_response)
    # Parse the JSON response
    geocode_content = json.loads(geocode_response.content)
    lat = geocode_content[0]["lat"]
    lon = geocode_content[0]["lon"]

    # Create the URL for the current weather API with the latitude andlongitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    weather_response = requests.get(weather_url)
    # Parse the JSON response
    weather_content = json.loads(weather_response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    try:
        return {
            "temperature": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return {"weather": None}
